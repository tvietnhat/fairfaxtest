//
//  NetworkClient.h
//  
//
//  Created by Nhat Tran on 19/11/12.
//
//

#import <Foundation/Foundation.h>


@interface NetworkClient : NSObject

+ (NetworkClient *) defaultClient;

- (NSData *) dataWithURLRequest:(NSURLRequest *)request error:(NSError **)error;
- (NSData *) dataWithURL:(NSString *)url error:(NSError **)error;
- (NSData *) dataWithURL:(NSString *)url timeoutInterval:(NSTimeInterval)timeoutInterval error:(NSError **)error;
- (id) jsonObjectWithURL:(NSString *)url error:(NSError **)error;

@end
