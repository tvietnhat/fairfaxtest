//
//  ArticleDataAdapter.m
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import "ArticleDataAdapter.h"
#import "NetworkClient.h"
#import "ArticleCollection.h"


#define ARTICLE_COLLECTION_FEED_URL @"http://mobilatr.mob.f2.com.au/services/views/9.json"

@implementation ArticleDataAdapter

- (ArticleCollection *)getArticleCollection:(NSError **)error {
    NSDictionary *dictionary = [[NetworkClient defaultClient] jsonObjectWithURL:ARTICLE_COLLECTION_FEED_URL error:error];
    
    ArticleCollection *articleCollection = nil;
    
    if (!*error && dictionary) {
        articleCollection = [[[ArticleCollection alloc] init] autorelease];
        [articleCollection loadFromDictionary:dictionary];
    }
    
    return articleCollection;
}

@end
