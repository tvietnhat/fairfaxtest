//
//  ArticleCollectionLoadOperation.h
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ArticleCollection;

@interface ArticleCollectionLoadOperation : NSOperation

@property (nonatomic, retain) ArticleCollection *articleCollection;
@property (nonatomic, retain) NSError *error;

@end
