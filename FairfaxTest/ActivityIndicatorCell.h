//
//  ActivityIndicatorCell.h
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicatorCell : UITableViewCell

@property (nonatomic, readonly) UIActivityIndicatorView *activityIndicator;

@end
