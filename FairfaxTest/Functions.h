//
//  Functions.h
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

#define __iOS7AndAbove (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
#define __iOS6AndBelow (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_6_1)

#pragma mark - common functions

extern id isNil(id obj, id replacementObj);
extern id isNSNull(id obj, id replacementObj);