//
//  ImageDownloader.h
//  BargainEveryday
//
//  Created by Nhat Tran on 21/09/10.
//  Copyright 2010 Rubik Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataDownloader.h"

@interface ImageDownloader : DataDownloader {
	UIImage *image;
}

@property (nonatomic, retain) UIImage *image;

@end

