//
//  AppDelegate.h
//  FairfaxTest
//
//  Created by Nhat Tran on 12/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (retain, nonatomic) UIWindow *window;

@end
