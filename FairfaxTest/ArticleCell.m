//
//  ArticleCell.m
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import "ArticleCell.h"
#import "Article.h"
#import "UILazyImageView.h"
#import "NSString+Extend.h"
#import "UIView+Extend.h"
#import "ColorGradientView.h"


#define TITLE_FONE_SIZE 15
#define CONTENT_TEXT_FONT_SIZE 13
#define CONTENT_MARGIN 6
#define THUMB_IMAGE_WIDTH 75
#define THUMB_IMAGE_HEIGHT 50
#define PARAGRAPH_SPACING 3
#define LABEL_DATE_HEIGHT 14

@implementation ArticleCell

@synthesize article = _article;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
        thumbnailView = [[UILazyImageView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        thumbnailView.contentMode = UIViewContentModeScaleAspectFit;
        thumbnailView.layer.borderWidth = 1;
        thumbnailView.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0].CGColor;
        [self.contentView addSubview:thumbnailView];
        
        labelHeadline = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        labelHeadline.backgroundColor = [UIColor clearColor];
        labelHeadline.textColor = [UIColor colorWithRed:62/255.0 green:91/255.0 blue:145/255.0 alpha:1.0];
        labelHeadline.numberOfLines = 0;
        labelHeadline.font = [UIFont boldSystemFontOfSize:TITLE_FONE_SIZE];
        [self.contentView addSubview:labelHeadline];
        
        labelSlugLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        labelSlugLine.backgroundColor = [UIColor clearColor];
        labelSlugLine.textColor = [UIColor colorWithWhite:116/255.0 alpha:1.0];
        labelSlugLine.numberOfLines = 0;
        labelSlugLine.font = [UIFont boldSystemFontOfSize:CONTENT_TEXT_FONT_SIZE];
        [self.contentView addSubview:labelSlugLine];
        
        labelDate = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        labelDate.backgroundColor = [UIColor clearColor];
        labelDate.textColor = [UIColor colorWithWhite:156/255.0 alpha:1.0];
        labelDate.font = [UIFont boldSystemFontOfSize:CONTENT_TEXT_FONT_SIZE];
        [self.contentView addSubview:labelDate];
        
        // create background view
        ColorGradientView *gradientView = [[ColorGradientView alloc] initWithStartingColor:[UIColor whiteColor] endingColor:[UIColor colorWithWhite:243/255.0 alpha:1.0] angle:270];
        gradientView.frame = self.bounds;
        self.backgroundView = gradientView;
        [gradientView release];
    }
    
    return self;
}

- (void)dealloc
{
    [labelHeadline release];
    [labelSlugLine release];
    [labelDate release];
    
    [_article release];
    
    [super dealloc];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat nextY = CONTENT_MARGIN;
    
    // set frame for label headline
    CGFloat headlineWidth = self.contentView.width - CONTENT_MARGIN*2;
    CGSize headLineSize = [labelHeadline sizeThatFits:CGSizeMake(headlineWidth, CGFLOAT_MAX)];
    labelHeadline.frame = CGRectMake(CONTENT_MARGIN, nextY, headlineWidth, headLineSize.height);
    nextY = labelHeadline.bottom + PARAGRAPH_SPACING;
    
    // set frame for thumbnail image view
    thumbnailView.frame = CGRectMake(self.contentView.width - CONTENT_MARGIN - THUMB_IMAGE_WIDTH, nextY, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT);
    
    // set frame for label slugline
    CGFloat slugLineWidth = self.contentView.width - CONTENT_MARGIN*2 - (thumbnailView.hidden ? 0 : thumbnailView.width);
    CGSize slugLineSize = [labelSlugLine sizeThatFits:CGSizeMake(slugLineWidth, CGFLOAT_MAX)];
    labelSlugLine.frame = CGRectMake(CONTENT_MARGIN, nextY, slugLineWidth, slugLineSize.height);
    nextY = labelSlugLine.bottom + PARAGRAPH_SPACING;
    
    // set frame for label date
    labelDate.frame = CGRectMake(CONTENT_MARGIN, nextY, slugLineWidth, LABEL_DATE_HEIGHT);
}

- (void)setArticle:(Article *)article
{
    // make sure this methods is called on main thread
    assert([NSThread isMainThread]);
    
    if (article != _article)
    {
        [_article release], _article = [article retain];
        [self reloadData];
    }
}

- (void) reloadData
{
    thumbnailView.imageURL = self.article.thumbnailImageHref;
    thumbnailView.hidden = [NSString isNullOrEmpty:self.article.thumbnailImageHref];
    
    labelHeadline.text = self.article.headLine;
    labelSlugLine.text = self.article.slugLine;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEE d MMM yyyy h:mm a";
    labelDate.text = [dateFormatter stringFromDate:self.article.dateLine];
    [dateFormatter release];
}

+ (CGFloat) tableView:(UITableView *)tableView rowHeightForArticle:(Article *)article accessoryViewWidth:(CGFloat)accessoryViewWidth
{
    CGFloat rowHeight = CONTENT_MARGIN * 2;
    
    // calculate headline height
    CGFloat headlineWidth = tableView.frame.size.width - CONTENT_MARGIN*2 - accessoryViewWidth;
    CGSize headLineSize = [article.headLine sizeWithFont:[UIFont boldSystemFontOfSize:TITLE_FONE_SIZE] constrainedToSize:CGSizeMake(headlineWidth, CGFLOAT_MAX)];
    rowHeight += headLineSize.height + PARAGRAPH_SPACING;
    
    CGFloat sligLineWidth = [NSString isNullOrEmpty:article.thumbnailImageHref] ? headlineWidth : headlineWidth - THUMB_IMAGE_WIDTH;
    CGSize slugLineSize = [article.slugLine sizeWithFont:[UIFont boldSystemFontOfSize:CONTENT_TEXT_FONT_SIZE] constrainedToSize:CGSizeMake(sligLineWidth, CGFLOAT_MAX)];
    rowHeight += slugLineSize.height + PARAGRAPH_SPACING;
    
    rowHeight += LABEL_DATE_HEIGHT;
    
    return rowHeight;
}

@end
