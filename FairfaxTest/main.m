//
//  main.m
//  FairfaxTest
//
//  Created by Nhat Tran on 12/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    NSAutoreleasePool *autoreleasePool = [[NSAutoreleasePool alloc] init];

    int result = UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));

    [autoreleasePool drain];
    
    return result;
}
