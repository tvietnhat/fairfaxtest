//
//  Article.h
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject

@property (nonatomic, retain) NSString *webHref;
@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *headLine;
@property (nonatomic, retain) NSString *slugLine;
@property (nonatomic, retain) NSDate *dateLine;
@property (nonatomic, retain) NSString *tinyUrl;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *thumbnailImageHref;

- (void) loadFromDictionary:(NSDictionary *)dictionary;

@end
