//
//  CryptoProvider.m
//  
//
//  Created by Nhat Tran on 7/06/12.
//  Copyright (c) 2012 . All rights reserved.
//

#import "CryptoProvider.h"
#import <CommonCrypto/CommonDigest.h>
#import "Base64.h"


@implementation CryptoProvider

+ (CryptoProvider *)defaultProvider {
    static dispatch_once_t onceToken;
    static CryptoProvider *defaultProvider = nil;
    dispatch_once(&onceToken, ^{
        defaultProvider = [[CryptoProvider alloc] init];
    });
    return defaultProvider;
}

- (NSData *) getSHA1HashBytes:(NSData *)data {
	CC_SHA1_CTX ctx;
	uint8_t * hashBytes = NULL;
	NSData * hash = nil;
	
	// Malloc a buffer to hold hash.
	hashBytes = malloc( CC_SHA1_DIGEST_LENGTH * sizeof(uint8_t) );
	memset((void *)hashBytes, 0x0, CC_SHA1_DIGEST_LENGTH);
	
	// Initialize the context.
	CC_SHA1_Init(&ctx);
	// Perform the hash.
	CC_SHA1_Update(&ctx, (void *)[data bytes], [data length]);
	// Finalize the output.
	CC_SHA1_Final(hashBytes, &ctx);
	
	// Build up the SHA1 blob.
	hash = [NSData dataWithBytes:(const void *)hashBytes length:(NSUInteger)CC_SHA1_DIGEST_LENGTH];
	
	if (hashBytes) free(hashBytes);
	
	return hash;
}

- (NSString *) getSHA1HashString:(NSData *)data {
    NSData *hashData = [self getSHA1HashBytes:data];
    NSString *string = [Base64 encode:hashData];;//[[[NSString alloc] initWithData:hashData encoding:NSASCIIStringEncoding] autorelease];
    return string;
}

- (NSString *) getSHA1HashForString:(NSString *)text encoding:(NSStringEncoding)encoding {
    NSData *data= [text dataUsingEncoding:encoding];
    return [self getSHA1HashString:data];
}

- (NSString *) getMD5HashForString:(NSString *)text {
    if(self == nil || [text length] == 0)
        return nil;
    
    const char *value = [text UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return outputString;
}

@end

