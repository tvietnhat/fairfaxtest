//
//  ColorGradientView.h
//  
//
//  Created by Nhat Tran on 3/07/12.
//  Copyright (c) 2012 . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorGradientView : UIView

@property (nonatomic, assign) CGFloat angle;

-(id)initWithStartingColor:(UIColor *)startingColor endingColor:(UIColor *)endingColor angle:(CGFloat)angle;
-(id)initWithStartingColor:(UIColor *)startingColor endingColor:(UIColor *)endingColor;

@end
