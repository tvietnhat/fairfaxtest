//
//  ArticleCell.h
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Article;
@class UILazyImageView;

@interface ArticleCell : UITableViewCell {
    UILabel *labelHeadline;
    UILabel *labelSlugLine;
    UILabel *labelDate;
    UILazyImageView *thumbnailView;
}

@property (nonatomic, retain) Article *article;

+ (CGFloat) tableView:(UITableView *)tableView rowHeightForArticle:(Article *)article accessoryViewWidth:(CGFloat)accessoryViewWidth;

@end
