//
//  UIView+Extend.m
//  
//
//  Created by Nhat Tran on 18/08/10.
//  Copyright 2010 Nhat Tran. All rights reserved.
//

#import "UIView+Extend.h"



@interface UIView (PrivateMethods) 

- (void) addRespondersToArray:(NSMutableArray *)array;

@end


@implementation UIView (PrivateMethods)

- (void) addRespondersToArray:(NSMutableArray *)array {
	if ([self canBecomeFirstResponder]) {
		[array addObject:self];
	}
	if ([self.subviews count] > 0) {
		for (UIView *subView in self.subviews) {
			[subView addRespondersToArray:array];
		}
	}
}

- (void) addGestureRecognizersToArray:(NSMutableArray *)recognizers {
    if (self.gestureRecognizers && self.gestureRecognizers.count > 0) {
        [recognizers addObjectsFromArray:self.gestureRecognizers];
    }
    for (UIView *subView in self.subviews) {
        [subView addGestureRecognizersToArray:recognizers];
    }
}


@end



@implementation UIView (Extend)



- (NSArray *) responders {
	NSMutableArray *array = [NSMutableArray array];
	[self addRespondersToArray:array];
	
	return array;
}

- (NSArray *) gestureRecognizersIncludingSubviews {
    NSMutableArray *recognizers = [[NSMutableArray alloc] init];
    [self addGestureRecognizersToArray:recognizers];
    NSArray *array = [NSArray arrayWithArray:recognizers];
    [recognizers release];
    
    return array;
}

- (void) removeAllSubviews {
    if (self.subviews) {
        NSArray *array = [[NSArray alloc] initWithArray:self.subviews];
        for (UIView *view in array) {
            [view removeFromSuperview];
        }
        [array release];
    }
}

- (CGFloat) left {
    return self.frame.origin.x;
}

- (CGFloat) top {
    return self.frame.origin.y;
}

- (CGFloat) bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (CGFloat) right {
    return self.frame.origin.x + self.frame.size.width;
}

- (CGFloat) width {
    return self.frame.size.width;
}

- (CGFloat) height {
    return self.frame.size.height;
}

- (CGFloat) boundsLeft {
    return self.bounds.origin.x;
}

- (CGFloat) boundsTop {
    return self.bounds.origin.y;
}

- (CGFloat) boundsBottom {
    return self.bounds.origin.y + self.bounds.size.height;
}

- (CGFloat) boundsRight {
    return self.bounds.origin.x + self.bounds.size.width;
}

- (CGFloat) boundsWidth {
    return self.bounds.size.width;
}

- (CGFloat) boundsHeight {
    return self.bounds.size.height;
}



@end

