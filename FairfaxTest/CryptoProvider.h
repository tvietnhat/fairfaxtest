//
//  CryptoProvider.h
//  
//
//  Created by Nhat Tran on 7/06/12.
//  Copyright (c) 2012 . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CryptoProvider : NSObject

- (NSData *) getSHA1HashBytes:(NSData *)data;
- (NSString *) getSHA1HashString:(NSData *)data;
- (NSString *) getSHA1HashForString:(NSString *)text encoding:(NSStringEncoding)encoding;
- (NSString *) getMD5HashForString:(NSString *)text;

+ (CryptoProvider *) defaultProvider;

@end
