//
//  UILazyImageView.m
//  FairfaxTest
//
//  Created by Nhat Tran on 16/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import "UILazyImageView.h"
#import "ImageDownloader.h"


@interface UILazyImageView () <DataDownloaderDelegate>

@property (nonatomic, retain) ImageDownloader *imageDownloader;

@end


@implementation UILazyImageView

@synthesize imageDownloader = _imageDownloader;

@synthesize imageURL = _imageURL;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc
{
    [self cancelCurrentDownload];
    
    _imageDownloader.delegate = nil, [_imageDownloader release];
    [_imageURL release];
    
    [super dealloc];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


#pragma mark - other methods

- (void)setImageURL:(NSString *)imageURL {
    // make sure this methods is called on main thread
    assert([NSThread isMainThread]);
    
    if (imageURL != _imageURL) {
        [_imageURL release], _imageURL = [imageURL retain];
        [self cancelCurrentDownload];
        [self reloadImage];
    }
}

- (void) cancelCurrentDownload {
    [self.imageDownloader cancelDownload];
    self.imageDownloader.delegate = nil;
    self.imageDownloader = nil;
}

- (void) reloadImage {
    self.image = nil;
    
    if (self.imageURL) {
        self.imageDownloader = [[[ImageDownloader alloc] initWithUrl:self.imageURL] autorelease];
        self.imageDownloader.delegate = self;
        [self.imageDownloader startDownload];
    }
}

#pragma mark - DataDownloaderDelegate

- (void)dataDownloaderDidFinish:(DataDownloader *)downloader {
    if (downloader == self.imageDownloader) {
        
        // display downloaded image
        self.image = self.imageDownloader.image;
        
        // dispose downloader object
        self.imageDownloader.delegate = nil;
        self.imageDownloader = nil;
    }
}

- (void)dataDownloaderDidFail:(DataDownloader *)downloader withError:(NSError *)error {
    if (downloader == self.imageDownloader) {
        // dispose downloader object
        self.imageDownloader.delegate = nil;
        self.imageDownloader = nil;
    }
}


@end
