//
//  ArticleCollection.h
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArticleCollection : NSObject

@property (nonatomic, retain) NSNumber *identifier;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSArray *items;

- (void) loadFromDictionary:(NSDictionary *)dictionary;

@end
