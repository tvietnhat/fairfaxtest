//
//  NSString+Extend.m
//  
//
//  Created by Nhat Tran on 25/06/10.
//  Copyright 2010 Nhat Tran. All rights reserved.
//

#import "NSString+Extend.h"


NSString * const kEmptyString = @"";


@implementation NSString (Extend)

- (BOOL) isEmpty {
	return [self length] == 0;
}

- (BOOL) containsCharacter:(unichar)character {
	BOOL contains = NO;
	for (NSUInteger i = 0; i < [self length]; i++) {
		if ([self characterAtIndex:i] == character) {
			contains = YES;
			break;
		}
	}
	
	return contains;
}

- (BOOL) startsWith:(NSString *)string options:(NSStringCompareOptions)mask {
	if ([self length] >= [string length]) {
		return [self rangeOfString:string options:mask range:NSMakeRange(0, [string length])].location != NSNotFound;
	} else {
		return NO;
	}

}

- (BOOL) endsWith:(NSString *)string options:(NSStringCompareOptions)mask {
	if ([self length] >= [string length]) {
		return [self rangeOfString:string options:mask range:NSMakeRange([self length] - [string length], [string length])].location != NSNotFound;
	} else {
		return NO;
	}
}

+ (NSString *) emptyString {
	return kEmptyString;
}

+ (BOOL) isNullOrEmpty:(NSString *)aString {
	return aString == nil || [aString length] == 0;
}


- (NSString *) stringByEncodingURL {
    CFStringRef result = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                                           (CFStringRef)self,
                                                                           NULL,
																		   CFSTR("!*'\"();:@&=+$,/?%#[]% "),
                                                                           kCFStringEncodingUTF8);
    NSString *string = [NSString stringWithString:(__bridge NSString *)result];
    CFRelease(result);
	return string;
}

- (NSString*) stringByDecodingURL {
	CFStringRef result = CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,
																						   (CFStringRef)self,
																						   CFSTR(""),
																						   kCFStringEncodingUTF8);
    NSString *string = [NSString stringWithString:(__bridge NSString *)result];
    CFRelease(result);
	return string;
}



@end
