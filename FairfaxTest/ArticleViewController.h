//
//  ArticleViewController.h
//  FairfaxTest
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Article;

@interface ArticleViewController : UIViewController

@property (nonatomic, retain) Article *article;

@end
