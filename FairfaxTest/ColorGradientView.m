//
//  ColorGradientView.m
//  
//
//  Created by Nhat Tran on 3/07/12.
//  Copyright (c) 2012 . All rights reserved.
//

#import "ColorGradientView.h"
#import "LBGradient.h"


@interface ColorGradientView ()

@property (nonatomic, readonly) LBGradient *gradient;

@end

@implementation ColorGradientView

@synthesize angle = _angle;
@synthesize gradient = _gradient;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.contentMode = UIViewContentModeRedraw;
    }
    return self;
}

- (id)initWithStartingColor:(UIColor *)startingColor endingColor:(UIColor *)endingColor angle:(CGFloat)angle {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        _angle = angle;
        _gradient = [[LBGradient alloc] initWithStartingColor:startingColor endingColor:endingColor];
        self.contentMode = UIViewContentModeRedraw;
    }
    return self;
}

- (id)initWithStartingColor:(UIColor *)startingColor endingColor:(UIColor *)endingColor {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        _angle = 90;
        _gradient = [[LBGradient alloc] initWithStartingColor:startingColor endingColor:endingColor];
        self.contentMode = UIViewContentModeRedraw;
    }
    return self;
}




// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [self.gradient drawInRect:self.bounds angle:self.angle];
}

- (void)setAngle:(CGFloat)angle {
    if (angle != _angle) {
        _angle = angle;
        [self setNeedsDisplay];
    }
}

@end

