//
//  NetworkClient.m
//
//
//  Created by Nhat Tran on 19/11/12.
//
//

#import "NetworkClient.h"


#define DEFAULT_REQUEST_TIMEOUT 60


@interface NetworkClient ()

@end


@implementation NetworkClient


+ (NetworkClient *)defaultClient {
    static NetworkClient *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[NetworkClient alloc] init];
    });
    
    return _instance;
}

- (id)init {
    self = [super init];
    
    if (self) {
    }
    
    return self;
}


- (void)dealloc {
    [super dealloc];
}


- (NSData *) dataWithURLRequest:(NSURLRequest *)request error:(NSError **)error {
    
    NSData *data = nil;
    NSURLResponse *response = nil;
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    @try {
        data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:error];
    }
    @catch (NSException *exception) {
        NSLog(@"Error when downloading data for url: %@\n%@", [[request URL] absoluteString], [exception description]);
    }
    @finally {
    }
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    
    return data;
}


- (NSData *) dataWithURL:(NSString *)url error:(NSError **)error {
    return [self dataWithURL:url timeoutInterval:0 error:error];
}


- (NSData *) dataWithURL:(NSString *)url timeoutInterval:(NSTimeInterval)timeoutInterval error:(NSError **)error {
	NSLog(@"dataWithURL %@", url);
    
	NSURL *URL = [NSURL URLWithString:url];
	
    // create the request object
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
	[request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    if (timeoutInterval > 0) {
        [request setTimeoutInterval:timeoutInterval];
    } else {
        [request setTimeoutInterval:DEFAULT_REQUEST_TIMEOUT];
    }
    
	return [self dataWithURLRequest:request error:error];
}

- (id)jsonObjectWithURL:(NSString *)url error:(NSError **)error {
    NSData *data = [self dataWithURL:url error:error];
    
    id jsonObject = nil;
    
    if (!*error && data) {
        jsonObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
    }
    
    return jsonObject;
}

@end

