//
//  UIView+Extend.h
//  
//
//  Created by Nhat Tran on 18/08/10.
//  Copyright 2010 Nhat Tran. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface UIView (Extend) 

- (NSArray *) responders;
- (NSArray *) gestureRecognizersIncludingSubviews;
- (void) removeAllSubviews;

@property (readonly) CGFloat top;
@property (readonly) CGFloat left;
@property (readonly) CGFloat bottom;
@property (readonly) CGFloat right;
@property (readonly) CGFloat width;
@property (readonly) CGFloat height;

@property (readonly) CGFloat boundsTop;
@property (readonly) CGFloat boundsLeft;
@property (readonly) CGFloat boundsBottom;
@property (readonly) CGFloat boundsRight;
@property (readonly) CGFloat boundsWidth;
@property (readonly) CGFloat boundsHeight;


@end
